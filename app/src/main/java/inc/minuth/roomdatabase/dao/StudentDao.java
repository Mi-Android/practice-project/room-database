package inc.minuth.roomdatabase.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import inc.minuth.roomdatabase.model.Student;

@Dao
public interface StudentDao
{
    @Insert
    void add(Student student);

    @Query("select * from student")
    List<Student>getAll();
}
