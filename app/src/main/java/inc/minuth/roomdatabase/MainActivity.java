package inc.minuth.roomdatabase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import inc.minuth.roomdatabase.dao.StudentDao;
import inc.minuth.roomdatabase.database.AppDatabase;
import inc.minuth.roomdatabase.model.Student;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppDatabase database=AppDatabase.getDatabase(getApplicationContext());
        StudentDao studentDao=database.studentDao();
        studentDao.add(new Student("minuth"));
        studentDao.add(new Student("dara"));

        Log.d("STUDENT_INFO",studentDao.getAll().get(0).toString());
    }
}
