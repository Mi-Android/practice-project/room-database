package inc.minuth.roomdatabase.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import inc.minuth.roomdatabase.dao.StudentDao;
import inc.minuth.roomdatabase.model.Student;

@Database(entities = {Student.class},version = 1)
public abstract class AppDatabase extends RoomDatabase
{
    private static AppDatabase INSTANCE;
    public abstract StudentDao studentDao();
    public static AppDatabase getDatabase(Context context)
    {
        if(INSTANCE==null)
        {
            INSTANCE=Room.databaseBuilder(context,AppDatabase.class,"school_db")
                    .fallbackToDestructiveMigration().build();
        }
        return INSTANCE;
    }
    public void destroyDatabase()
    {
        INSTANCE=null;
    }

}
